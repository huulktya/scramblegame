ScrambleCard
============

This is my version of the Sliding puzzle.
It currently supports self solving, resizing
of the dimensions of the puzzle, and changing
the picture on the puzzle. More features to
come!

To run the app download the repository and run EightPuzzle.jar.
